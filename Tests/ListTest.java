import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ListIterator;

public class ListTest {
    private TwoWayLinkedList emptyList;
    private TwoWayLinkedList filledList;

    @Before
    public void setup(){
        String[] testStrings = {"hei", "hallo", "god kveld"};
        emptyList = new TwoWayLinkedList();
        filledList = new TwoWayLinkedList(testStrings);
    }

    @Test
    public void addAndCheckCorrectIndexTest(){
        emptyList.add(3, "hei");
        emptyList.add(0, "hallo");
        emptyList.add(1, "hallais");
        emptyList.add(2, "god kveld");
        emptyList.add(1, "god ");
        emptyList.add(0, "god kveld");
        assertEquals("god kveld", emptyList.get(0));
    }

    @Test
    public void removeTest(){
        emptyList.add(0, "hallo");
        emptyList.add(2, "hei");
        emptyList.add(1, "halelo");
        emptyList.add(2, "hallaao");
        emptyList.remove(0);
        emptyList.remove(0);

        assertEquals("hei", emptyList.get(1));
    }

    @Test
    public void addFirstTest(){
        emptyList.addFirst("hei");
        assertEquals("hei", emptyList.get(0));
    }

    @Test
    public void addArray(){
        assertEquals("hei", filledList.get(0));
    }

    @Test
    public void getNextElement(){
        ListIterator myItr = filledList.listIterator();
        assertEquals("hallo", myItr.next());
    }

    @Test
    public void getPrevElement(){
        ListIterator myItr = filledList.listIterator();
        myItr.next();
        assertEquals("hei", myItr.previous());
    }

    @Test
    public void hasPreviousTest(){
        ListIterator myItr = filledList.listIterator();
        myItr.next();
        myItr.next();
        assertEquals(true, myItr.hasPrevious());
    }

    @Test
    public void hasNextTest() {
        ListIterator myItr = filledList.listIterator();
        assertEquals(true, myItr.hasNext());
    }

    @Test
    public void nextIndexTest(){
        ListIterator myItr = filledList.listIterator();
        myItr.next();
        assertEquals(2, myItr.nextIndex());
    }

    @Test
    public void prevIndexTest(){
        ListIterator myItr = filledList.listIterator();
        myItr.next();
        assertEquals(0, myItr.previousIndex());
    }

    @Test
    public void removeItrTest(){
        ListIterator myItr = filledList.listIterator();
        myItr.next();
        myItr.remove();
        assertEquals("god kveld", filledList.get(1));
    }
}
