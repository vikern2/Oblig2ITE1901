import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import static org.junit.Assert.*;

public class BSTTest {
    private BST<Integer> preFilledBST;
    private Integer[] rndmNbrs = {10,50,3,4,19,7};
    private BST emptyBST;
    private BST<Integer> fullBST;
    private Integer[] fullBSTnbrs = {90, 50, 150, 20, 75, 95, 175, 5, 25, 66, 80, 92, 111, 166, 200};

    @Before
    public void setup(){
        emptyBST = new BST();
        preFilledBST = new BST(rndmNbrs);
        fullBST = new BST(fullBSTnbrs);
    }

    @Test
    public void insertTest(){
        emptyBST.insert(7);
        emptyBST.insert(3);
        emptyBST.insert(9);
        emptyBST.insert(8);
        assertEquals(9, emptyBST.getNode(8).parent.element);
    }

    @Test
    public void deleteTest(){
        emptyBST.insert(7);
        emptyBST.insert(3);
        emptyBST.insert(1);
        emptyBST.delete(3);
        assertEquals(7, emptyBST.getNode(1).parent.element);
        assertEquals(null, emptyBST.getNode(1).left);
    }

    @Test
    public void deleteRoot(){
        assertEquals(true, preFilledBST.delete(10));
        assertEquals(false, preFilledBST.search(10));
        assertEquals(7, (int)preFilledBST.getRoot().element);
    }

    @Test
    public void deleteLeaf(){
        assertEquals(true, preFilledBST.delete(7));
        assertEquals(false, preFilledBST.search(7));
    }

    @Test
    public void deleteRightOfRoot(){
        assertEquals(true, preFilledBST.delete(50));
        assertEquals(false, preFilledBST.search(50));
        assertEquals(19, (int)preFilledBST.getRoot().right.element);
    }

    @Test
    public void isLeafTest(){
        assertEquals(true, preFilledBST.isLeaf(19));
        assertEquals(false, preFilledBST.isLeaf(50));
    }

    @Test
    public void getNodeTest(){
        assertEquals(19, (int) preFilledBST.getNode(19).element);
    }

    @Test
    public void getPathTest(){
        ArrayList<Integer> compareList = new ArrayList<>();
        compareList.add(4);
        compareList.add(3);
        compareList.add(10);
        assertEquals(compareList, preFilledBST.getPath(4));
    }

    @Test
    public void addAllTest(){
        emptyBST.addAll(rndmNbrs);
        assertEquals(6, emptyBST.getSize());
        assertEquals(true, emptyBST.isLeaf(19));
        assertEquals(10, emptyBST.getRoot().element);
    }

    @Test
    public void preorderTest(){
        ArrayList<Integer> tmp = new ArrayList<>();
        Integer[] tmpArr = {10, 3, 4, 7, 50, 19};
        tmp.addAll(Arrays.asList(tmpArr));
        Iterator<Integer> myItr = preFilledBST.preorderIterator();
        assertEquals(tmp, preFilledBST.getList());
    }

    @Test
    public void getRootTest(){
        int tmp = (int) preFilledBST.getRoot().element;
        assertEquals(preFilledBST.getNode(tmp), preFilledBST.getRoot());
    }

    @Test
    public void preorderRemoveTest(){
        Iterator<Integer> myItr = preFilledBST.preorderIterator();
        myItr.next();
        myItr.remove();
        assertEquals(false, preFilledBST.search(3));
    }

    @Test
    public void preorderRemovePreorderTest(){
        ArrayList<Integer> tmp = new ArrayList<>();
        Integer[] tmpArr = {10, 4, 7, 50, 19};
        tmp.addAll(Arrays.asList(tmpArr));
        Iterator<Integer> myItr = preFilledBST.preorderIterator();
        myItr.next();
        myItr.remove();
        assertEquals(tmp, preFilledBST.getList());
    }

    @Test
    public void searchTest(){
        assertEquals(true, preFilledBST.search(19));
    }

    @Test
    public void getSizeTest(){
        assertEquals(6, preFilledBST.getSize());
    }

    @Test
    public void getLeavesTest(){
        ArrayList<Integer> tmp = new ArrayList<>();
        Integer[] tmpArr = {19, 7};
        tmp.addAll(Arrays.asList(tmpArr));
        assertEquals(tmp, preFilledBST.getLeaves());

    }

    @Test
    public void isFullTest(){
        assertEquals(false, preFilledBST.isFull());
        assertEquals(true, fullBST.isFull());
    }

}
