import java.util.*;

public class TwoWayLinkedList<E> extends AbstractSequentialList<E> {
    private E[] elements;
    private Node head;
    private Node tail;
    private int size;

    TwoWayLinkedList() {
        size = 0;
    }

    TwoWayLinkedList(E[] elements) {
        size = 0;
        for (E element : elements) {
            add(element);
        }
    }

    @Override
    public E get(int index) {
        Node temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return (E) temp.element;
    }

    @Override
    public void add(int index, E e) {
        if (index == 0) {
            addFirst(e);
        } else if (size == 1 || size <= index) {
            addLast(e);
        } else {
            Node<E> temp = new Node<E>(e);
            Node<E> prev = getNode(index - 1);
            Node<E> nxt = getNode(index);
            temp.next = nxt;
            temp.previous = prev;
            prev.next = temp;
            nxt.previous = temp;

            size++;
        }
    }

    private Node<E> getNode(int index) {
        Node<E> temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    @Override
    public E remove(int index) {
        Node<E> prev;
        Node<E> next;
        Node<E> nodeToRemove = getNode(index);

        if (index == 0) {
            next = getNode(index + 1);
            next.previous = null;
            head = next;
        } else if (size > index) {
            prev = getNode(index - 1);
            prev.next = null;
            tail = prev;
        } else {
            prev = getNode(index - 1);
            next = getNode(index + 1);
            prev.next = next;
            next.previous = prev;
        }
        size--;
        return nodeToRemove.element;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        boolean modified = false;
        for (Object o : c) {
            add((E) o);
        }
        return modified;
    }

    @Override
    public int size() {
        return size;
    }

    void addFirst(E e) {
        Node temp = new Node(e, head, null);
        if (!isEmpty()) {
            head.previous = temp;
        }
        head = temp;
        if (isEmpty()) {
            tail = temp;
        }
        size++;
    }

    private void addLast(E e) {
        Node<E> temp = new Node(e, null, tail);
        if (!isEmpty()) {
            tail.next = temp;
        }
        tail = temp;
        if (isEmpty()) {
            head = temp;
        }
        size++;
    }

    @Override
    public Iterator iterator() {
        return super.iterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return new TwoWayLinkedListIterator<>();
    }


    class TwoWayLinkedListIterator<E> implements ListIterator<E> {
        Node<E> current = head;
        int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return (current.next != null);
        }

        @Override
        public E next() {
            if(hasNext()) {
                current = current.next;
                currentIndex++;
                return current.element;
            }else{
                System.out.println("Next index is empty");
                return current.element;
            }
        }

        @Override
        public boolean hasPrevious() {
            return (current.previous != null);
        }

        @Override
        public E previous() {
            if(hasPrevious()){
                current = current.previous;
                currentIndex--;
                return current.element;
            }else{
                System.out.println("There is no previous element");
                return current.element;
        }}

        @Override
        public int nextIndex() {
            int nextIndex = currentIndex+1;
            if(hasNext()) {
                return nextIndex;
            } else {
                return currentIndex;
            }
        }

        @Override
        public int previousIndex() {
            int prevIndex = currentIndex-1;
            if(hasPrevious()){
                return prevIndex;
            }else{
                return currentIndex;
            }
        }

        @Override
        public void remove() {
            TwoWayLinkedList.this.remove(currentIndex);
            if(hasNext()){
                current.next.previous = current.previous;
                current.previous.next = current.next;
            }else if(hasPrevious()){
                current.previous.next = null;
            }else{
                current = null;
                currentIndex = 0;
            }
        }

        @Override
        public void set(E e) {

        }

        @Override
        public void add(E e) {

        }
    }
}
