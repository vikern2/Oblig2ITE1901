public class Node<E> {
    E element;
    Node<E> next;
    Node<E> previous;

        public Node(E e){
            this.element = e;
        }

         public Node(E e, Node next, Node previous) {
             this.element = e;
             this.next = next;
             this.previous = previous;
         }}



