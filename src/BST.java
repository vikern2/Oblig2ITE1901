import java.util.ArrayList;

public class BST<E extends Comparable<E>> implements Tree<E> {
    private TreeNode<E> root;
    private int size = 0;
    private ArrayList<E> list = new ArrayList<E>();
    private ArrayList<E> elements = new ArrayList<E>();

    BST() {
    }

    BST(E[] objects) {
        for(int i = 0; i<objects.length; i++){
            elements.add(objects[i]);
        }
        addAll(objects);
    }

    @Override
    public boolean search(E e) {
        TreeNode<E> current = root; // Start from the root

        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                current = current.right;
            }
            else // element matches current.element
                return true; // Element is found
        }

        return false;
    }

    ArrayList<E> getList(){
        return list;
    }

    @Override
    public boolean insert(E e) {
        if (root == null)
            root = createNewNode(e, null); // Create a new root
        else {
            // Locate the parent node
            TreeNode<E> parent = null;
            TreeNode<E> current = root;
            while (current != null)
                if (e.compareTo(current.element) < 0) {
                    parent = current;
                    current = current.left;
                }
                else if (e.compareTo(current.element) > 0) {
                    parent = current;
                    current = current.right;
                }
                else
                    return false; // Duplicate node not inserted

            // Create the new node and attach it to the parent node
            if (e.compareTo(parent.element) < 0)
                parent.left = createNewNode(e, parent);
            else
                parent.right = createNewNode(e, parent);
        }

        size++;
        return true; // Element inserted successfully
    }

    private TreeNode<E> createNewNode(E e, TreeNode<E> p) {
        return new TreeNode<>(e, p);
    }

    @Override
    public void inorder() {
        inorder(root);
    }

    private void inorder(TreeNode<E> root) {
        if (root == null) return;
        inorder(root.left);
        System.out.print(root.element + " ");
        inorder(root.right);
    }

    @Override
    public void postorder() {
        postorder(root);
    }

    private void postorder(TreeNode<E> root) {
        if (root == null) return;
        postorder(root.left);
        postorder(root.right);
        System.out.print(root.element + " ");
    }

    @Override
    public void preorder() {
        preorder(root);
    }

    private void preorder(TreeNode<E> root) {
        if (root == null) return;
        System.out.print(root.element + " ");
        preorder(root.left);
        preorder(root.right);
    }

    ArrayList<E> getPath(E e) {
        ArrayList<E> path = new ArrayList<E>();
        TreeNode<E> tmp = getNode(e);
        while(tmp.parent != null) {
            path.add(tmp.element);
            tmp = tmp.parent;
        }
        path.add(getRoot().element);
        return path;
    }

    void addAll(E[] e) {
        for (E anE : e) insert(anE);
    }

    static class TreeNode<E> {
        E element;
        TreeNode<E> left;
        TreeNode<E> right;
        TreeNode<E> parent;

        TreeNode(E e, TreeNode<E> p) {
            element = e;
            parent = p;
        }
    }

    public ArrayList<E> getLeaves(){
        ArrayList<E> leafList = new ArrayList<>();
        for(int i = 0; i<elements.size(); i++){
            if(isLeaf(elements.get(i))){
                leafList.add(elements.get(i));
            }
        }
        return leafList;
    }

    public boolean isFull(){
        ArrayList<E> leafList = getLeaves();
        if(elements.size()-leafList.size() == leafList.size()-1){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    TreeNode<E> getNode(E e){
        TreeNode<E> parent = null;
        TreeNode<E> current = root;
        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                parent = current;
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                parent = current;
                current = current.right;
            }
            else
                break; // Element is in the tree pointed at by current
        }
        if (current == null) {
            return null; // Element is not in the tree
        }else{
            return current;
        }
    }

    TreeNode<E> getRoot() {
        return root;
    }

    public java.util.ArrayList<TreeNode<E>> path(E e) {
        java.util.ArrayList<TreeNode<E>> list =
                new java.util.ArrayList<>();
        TreeNode<E> current = root; // Start from the root

        while (current != null) {
            list.add(current); // Add the node to the list
            if (e.compareTo(current.element) < 0) {
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                current = current.right;
            }
            else
                break;
        }

        return list; // Return an array list of nodes
    }

    boolean isLeaf(E e){
        TreeNode<E> tmp = getNode(e);
        return tmp != null && tmp.right == null && tmp.left == null;
    }

    @Override
    public boolean delete(E e) {
        // Locate the node to be deleted and also locate its parent node
        TreeNode<E> parent = null;
        TreeNode<E> current = root;
        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                parent = current;
                current = current.left;
            }
            else if (e.compareTo(current.element) > 0) {
                parent = current;
                current = current.right;
            }
            else
                break; // Element is in the tree pointed at by current
        }

        if (current == null)
            return false; // Element is not in the tree

        // Case 1: current has no left child
        if (current.left == null) {
            // Connect the parent with the right child of the current node
            if (parent == null) {
                root = current.right;
                root.parent = null;
            } else {
                if (e.compareTo(parent.element) < 0) {
                    parent.left = current.right;
                    if(parent.left != null){
                    current.right.parent = parent;
                }} else {
                    parent.right = current.right;
                    if(parent.right != null){
                    current.right.parent = parent;
                }}
            }
        } else {
            // Case 2: The current node has a left child
            // Locate the rightmost node in the left subtree of
            // the current node and also its parent
            TreeNode<E> parentOfRightMost = current;
            TreeNode<E> rightMost = current.left;

            while (rightMost.right != null) {
                parentOfRightMost = rightMost;
                rightMost = rightMost.right; // Keep going to the right
            }

            // Replace the element in current by the element in rightMost
            current.element = rightMost.element;


            // Eliminate rightmost node
            if (parentOfRightMost.right == rightMost)
                parentOfRightMost.right = rightMost.left;
                if(parentOfRightMost.right != null){
                    rightMost.left.parent = parentOfRightMost;
                }
            else
                // Special case: parentOfRightMost == current
                parentOfRightMost.left = rightMost.left;
                if(parentOfRightMost.left != null){
                    parentOfRightMost.left.parent = parentOfRightMost;
                }
        }

        size--;
        return true; // Element deleted successfully
    }

    @Override
    public java.util.Iterator<E> iterator() {
        return new InorderIterator();
}

    // Inner class InorderIterator
    private class InorderIterator implements java.util.Iterator<E> {
        // Store the elements in a list
        private int current = 0; // Point to the current element in list

        InorderIterator() {
            inorder(); // Traverse binary tree and store elements in list
        }

        private void inorder() {
            list = new ArrayList<>();
            inorder(root);
        }

        private void inorder(TreeNode<E> root) {
            if (root == null)return;
            inorder(root.left);
            list.add(root.element);
            inorder(root.right);
        }

        @Override
        public boolean hasNext() {
            if (current < list.size())
                return true;

            return false;
        }

        @Override
        public E next() {
            return list.get(current++);
        }

        @Override
        public void remove() {
            delete(list.get(current)); // Delete the current element
            list.clear(); // Clear the list
            inorder(); // Rebuild the list
        }
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }
    PreorderIterator preorderIterator(){
        return new PreorderIterator();
    }

    private class PreorderIterator implements java.util.Iterator<E> {
        int index = 0;

        PreorderIterator(){
            preorder(root);
        }
        public void preorder() {
            list = new ArrayList<>();
            preorder(root);
        }

        private void preorder(TreeNode<E> root) {
            if (root == null) return;
            list.add(root.element);
            preorder(root.left);
            preorder(root.right);
        }

        @Override
        public boolean hasNext() {
            return (list.size() > index);
        }

        @Override
        public E next() {
            return list.get(index++);
        }

        @Override
        public void remove() {
            delete(list.get(index));
            list.clear();
            preorder();}
        }
    }
